import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`http://localhost:2000/user/getUserList`);
    }

    register(user: User,image :any ) {
        const formData = new FormData()
        formData.append('file', this.DataURIToBlob(image));
        formData.append('data', JSON.stringify(user));

        return this.http.post(`http://localhost:2000/user/add`, formData);
    }

    delete(id: number) {
        return this.http.delete(`/users/${id}`);
    }


    DataURIToBlob(dataURI: string) {
        const splitDataURI = dataURI.split(',')
        const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
        const mimeString = splitDataURI[0].split(':')[1].split(';')[0]
        
        const ia = new Uint8Array(byteString.length)
        for (let i = 0; i < byteString.length; i++)
            ia[i] = byteString.charCodeAt(i)
      
        return new Blob([ia], { type: mimeString })
      }
}